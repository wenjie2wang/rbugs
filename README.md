# rbugs

[![pipeline status][pkg-ci]][pkg-repo]

Functions to prepare files needed for running BUGS in batch-mode, and running
BUGS from R. Support for Linux and Windows systems with OpenBugs is emphasized.


[pkg-repo]: https://gitlab.com/wenjie2wang/rbugs/
[pkg-ci]: https://gitlab.com/wenjie2wang/rbugs/badges/master/pipeline.svg
