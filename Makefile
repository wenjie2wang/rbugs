objects := $(wildcard R/*.R) DESCRIPTION
version := $(shell grep "Version" DESCRIPTION | awk '{print $$NF}')
pkg := $(shell grep "Package" DESCRIPTION | awk '{print $$NF}')
tar := $(pkg)_$(version).tar.gz
checkLog := $(pkg).Rcheck/00check.log
dt := $(shell date +"%Y-%m-%d")

# rmd := vignettes/$(pkg)-intro.Rmd
# vignettes := vignettes/$(pkg)-intro.html


.PHONY: check
check: $(checkLog)

.PHONY: build
build: $(tar)

# .PHONY: preview
# preview: $(vignettes)

$(tar): $(objects)
	@Rscript -e "library(methods); devtools::document();";
	@$(MAKE) updateTimestamp
	R CMD build .

$(checkLog): $(tar)
	R CMD check --as-cran $(tar)

# $(vignettes): $(rmd)
#	Rscript -e "rmarkdown::render('$(rmd)')"

.PHONY: install
install: $(tar)
	R CMD INSTALL $(tar)

## update copyright year in HEADER, R script and date in DESCRIPTION
.PHONY: updateTimestamp
updateTimestamp:
	@bash misc/update_timestamp.sh

## make tags
.PHONY: TAGS
TAGS:
	Rscript -e "utils::rtags(path = 'R', ofile = 'TAGS')"
# gtags

.PHONY: clean
clean:
	@$(RM) -rf *~ */*~ *.Rhistroy *.tar.gz src/*.so src/*.o *.Rcheck/ .\#*
