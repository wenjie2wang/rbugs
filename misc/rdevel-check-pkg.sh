#!/bin/bash

# this script should be run only by gitlab-runner
# and by a container from rocker/r-devel

set -e

PKG=$(grep "Package" DESCRIPTION | awk '{print $NF}')
VERSION=$(grep "Version" DESCRIPTION | awk '{print $NF}')
TAR=$PKG\_$VERSION.tar.gz
LOG=$PKG.Rcheck/00check.log
RD CMD build .
if RD CMD check --as-cran $TAR
then
    # if no errors
    if egrep -q "WARNING" $LOG
    then
        printf "Found warning(s)!\n"
        exit 1
    else
        printf "No warning or error was found.\n"
        exit 0
    fi
else
    # with errors
    printf "Found Error(s)!\n"
    printf "\n Installation log:\n"
    cat $PKG.Rcheck/00install.out
    exit 1
fi
