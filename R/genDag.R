##' Generate DAG sequence command for CondorDAG
##'
##' The function is responsible to generate the dag sequence commands file for
##' CondorDAG.  It can only do linear sequence.
##' It generates Dag sumbission file.
##'
##' This function create the necessary structure in a CondorDAG submission file
##' to be executed at CondorDAG to run a proccess in multiple steps.
##'
##' @usage
##' genDag(nstep = 2, job_names = paste0("J", seq_len(nstep)),
##'        condor_name = "main.condor", dag_name = "main.dag",
##'        verbose = TRUE)
##'
##' @param nstep Number of steps to break the MCMC.
##' @param job_names Proccesses names, must be a vector of strings or character.
##' @param condor_name File name for the Condor sumbission(s) file(s) to be
##' included in the DAG file.
##' @param dag_name File name for the CondorDAG submission file.
##' @param verbose A logical value for verbose messages.
##'
##' @return \code{NULL} invisibly.
##'
##' @keywords utilities interface
##' @export
genDag <- function(nstep = 2,
                   job_names = paste0("J", seq_len(nstep)),
                   condor_name = "main.condor",
                   dag_name = "main.dag",
                   verbose = TRUE)
{
    if (nstep <= 2)
        stop("The number of steps must be at least two.")
    if (! missing(job_names) && length(job_names) != nstep)
        stop("The number of job names has to be equal to the number of steps.")

    text <- c(
        paste("JOB", job_names, condor_name),
        paste("PARENT", job_names[- nstep], "CHILD", job_names[- 1L])
    )
    write(text, file = dag_name)

    if (verbose)
        message(file.path(getwd(), dag_name), " is generated.\n")
    ## return
    invisible(NULL)
}
