##' Generate necessary Condor and R script files for CondorDAG
##'
##' The function is responsible to generate the submission Condor files as well
##' as the .R scripts from the template files.
##'
##' This function create the necessary files strucuture to be executed at
##' CondorDAG to run a proccess in multiple steps.
##'
##' @usage
##' genFiles4CondorDag(nstep = 1, files.to.pass = NULL,
##'                    pattern = '_nstep_', n.jobs = nstep, ...)
##' @param nstep Number of steps to break the MCMC.
##' @param files.to.pass Vector of string with the files that needs to be
##' sumbitted with the jobs. It might have a patterns to be substituted for
##' each step.
##' @param pattern Selected pattern for the passed files to Condor that must be
##' substituted on creation.
##' @param n.jobs Must be the same as name steps, used for unification of the
##' functions.
##' @param ...  Other genCondor() commands
##' @return Files created as output.
##' @author Jun Yan \email{jyan@@stat.uconn.edu} and Marcos O. Prates
##' \email{marcosop@@est.ufmg.br}
##' @keywords utilities interface
genFiles4CondorDag <- function(nstep = 1, files.to.pass = NULL,
                               pattern = "_nstep_", n.jobs = nstep, ...) {
    file.aux <- files.to.pass
    for (i in seq_len(nstep)) {
        ## Check wheter are files that depend on the step
        if (length(grep(pattern, files.to.pass)))
            file.aux <- gsub("_nstep_", i - 1, files.to.pass)
        genFiles4Condor(n.jobs = i - 1, files.to.pass = file.aux, ...)
    }
    invisible(NULL)
}
